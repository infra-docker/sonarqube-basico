# sonarqube-basico

Para windows 

[set max_map_count](https://stackoverflow.com/questions/69214301/using-docker-desktop-for-windows-how-can-sysctl-parameters-be-configured-to-per/69294687#69294687)

In your Windows %userprofile% directory (typically C:\Users\<username>) create or edit the file .wslconfig with the following:

```
[wsl2]
kernelCommandLine = "sysctl.vm.max_map_count=262144"
```

Then exit any WSL instance, wsl --shutdown, and restart.

```
> sysctl vm.max_map_count
vm.max_map_count = 262144
```

